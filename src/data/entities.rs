
use data::vector::Vector3;

#[derive(Debug,PartialEq)]
pub struct Sphere {
    pub radius: f32,
    pub center: Vector3
}
