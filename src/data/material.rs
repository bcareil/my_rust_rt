use data::color::ColorRGB;

#[derive(Clone)]
pub struct Material {
    pub specular_color: ColorRGB,
    pub diffuse_color: ColorRGB,
    pub ambiant_color: ColorRGB,
    pub reflective_factor: f32,
    pub transparent_factor: f32,
}

#[derive(Debug)]
pub enum Error {
    #[allow(dead_code)]
    DuplicateKey(String),
}

pub struct MaterialList {
    materials: Vec< Material >,
    keys: Vec< String >,
}

impl MaterialList {
    pub fn new() -> MaterialList {
        MaterialList {
            materials: Vec::new(),
            keys: Vec::new()
        }
    }

    pub fn get_index( &self, name: &str ) -> Option< usize > {
        for i in 0..self.keys.len() {
            if self.keys[ i ] == name {
                return Some( i );
            }
        }
        None
    }

    #[allow(dead_code)]
    pub fn contains( &self, name: &str ) -> bool {
        self.get_index( name ).is_some()
    }

    pub fn store( &mut self, name: &str, mat: Material ) {
        if let Some( i ) = self.get_index( name ) {
            self.materials[ i ] = mat;
        } else {
            self.keys.push( name.to_string() );
            self.materials.push( mat );
        }
    }

    pub fn get_material<'a>( &'a self, name: &str ) -> Option< &'a Material > {
        if let Some( i ) = self.get_index( name ) {
            Some( &self.materials[ i ] )
        } else {
            None
        }
    }

    pub fn apply< F: Fn( &mut Material ) -> () >( &mut self, functor: F ) {
        for i in 0..self.materials.len() {
            functor( &mut self.materials[ i ] );
        }
    }
}
