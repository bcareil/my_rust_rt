
#[derive(Clone,Debug,PartialEq)]
pub struct ColorRGB {
    pub r: f32,
    pub g: f32,
    pub b: f32,
}

macro_rules! colorrgb_u8_to_float {
    ($exp: expr) => (
        ($exp & 0xff) as f32 / 255.0
    )
}

macro_rules! colorrgb_float_to_u8 {
    ($exp: expr) => (
        ($exp * 255.0) as u32 & 0xff
    )
}

#[allow(dead_code)]
impl ColorRGB {
    pub fn from_u32_rgb(word: &u32) -> ColorRGB {
        ColorRGB {
            r: colorrgb_u8_to_float!((word >> 16) as i32),
            g: colorrgb_u8_to_float!((word >>  8) as i32),
            b: colorrgb_u8_to_float!((word >>  0) as i32),
        }
    }

    pub fn white() -> ColorRGB {
        ColorRGB { r: 1.0, g: 1.0, b: 1.0 }
    }

    pub fn new(r: f32, g: f32, b: f32) -> ColorRGB {
        ColorRGB { r: r, g: g, b: b }
    }

    pub fn bleed(&self) -> ColorRGB {
        let colors = vec![self.r, self.g, self.b];
        let bleeding: Vec<(usize, &f32)> = colors.iter().enumerate()
            .filter(|x| *x.1 > 1.0).collect();
        let blood: f32 = bleeding.iter().fold(0.0, |acc, x| acc + x.1 - 1.0);
        if blood == 0.0 {
            ColorRGB { r: self.r, g: self.g, b: self.b }
        } else {
            let blood = blood / (3 - bleeding.len()) as f32;
            let blood = 1.2 - (1.2 / (0.8 * blood + 1.0));
            let colors: Vec<f32> = colors.iter()
                .map(|&x| if x > 1.0 { x } else { x + blood })
                .map(|x| if x > 1.0 { 1.0 } else { x })
                .collect();
            ColorRGB { r: colors[0], g: colors[1], b: colors[2] }
        }
    }

    pub fn to_u32_rgb(&self) -> u32 {
        colorrgb_float_to_u8!(self.r) << 16
            | colorrgb_float_to_u8!(self.g) << 8
            | colorrgb_float_to_u8!(self.b)
    }
}

decl_op_v!(Add, add, ColorRGB, r, g, b);
decl_op_v!(Sub, sub, ColorRGB, r, g, b);
decl_op_v!(Mul, mul, ColorRGB, r, g, b);
decl_op_v!(Div, div, ColorRGB, r, g, b);

decl_op_s!(Add, add, f32, ColorRGB, r, g, b);
decl_op_s!(Sub, sub, f32, ColorRGB, r, g, b);
decl_op_s!(Mul, mul, f32, ColorRGB, r, g, b);
decl_op_s!(Div, div, f32, ColorRGB, r, g, b);

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_u32_rgb() {
        let color_u32 = 0xaabbccdd;
        let color_rgb = ColorRGB::from_u32_rgb(&color_u32);

        assert_eq!(color_rgb.r, (0xbb as f32 / 255.0));
        assert_eq!(color_rgb.g, (0xcc as f32 / 255.0));
        assert_eq!(color_rgb.b, (0xdd as f32 / 255.0));
    }

    #[test]
    fn test_color_mul_s() {
        let color = ColorRGB::white() * 0.5;

        assert_eq!(color.r, 0.5);
        assert_eq!(color.g, 0.5);
        assert_eq!(color.b, 0.5);
    }
}
