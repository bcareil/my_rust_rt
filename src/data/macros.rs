
macro_rules! init_member_from_binary_op_s {
    ($t: ident, $a: ident, $op: ident, $b: ident, $($x: ident),*) => (
        $t {
            $(
                $x: $a.$x.$op($b),
            )*
        }
    )
}

macro_rules! init_member_from_binary_op_v {
    ($t: ident, $a: ident, $op: ident, $b: ident, $($x: ident),*) => (
        $t {
            $(
                $x: $a.$x.$op($b.$x),
            )*
        }
    )
}

macro_rules! decl_op_impl_internal_binary_v {
    ($t: ident, $op: ident, $oth_type: ty, $($x: ident),*) => {
        type Output = $t;
        fn $op(self, oth: $oth_type) -> $t {
            init_member_from_binary_op_v!(
                $t, self, $op, oth, $($x),*
            )
        }
    }
}

macro_rules! decl_op_impl_internal_binary_s {
    ($t: ident, $op: ident, $oth_type: ty, $($x: ident),*) => {
        type Output = $t;
        fn $op(self, oth: $oth_type) -> $t {
            init_member_from_binary_op_s!(
                $t, self, $op, oth, $($x),*
            )
        }
    }
}

macro_rules! decl_op_v {
    ($cap_op: ident, $op: ident, $t: ident, $($x: ident),*) => (
        impl std::ops::$cap_op<$t> for $t {
            decl_op_impl_internal_binary_v!(
                $t, $op, $t, $($x),*
            );
        }

        impl<'a> std::ops::$cap_op<$t> for &'a $t {
            decl_op_impl_internal_binary_v!(
                $t, $op, $t, $($x),*
            );
        }

        impl<'b> std::ops::$cap_op<&'b $t> for $t {
            decl_op_impl_internal_binary_v!(
                $t, $op, &'b $t, $($x),*
            );
        }

        impl<'a, 'b> std::ops::$cap_op<&'b $t> for &'a $t {
            decl_op_impl_internal_binary_v!(
                $t, $op, &'b $t, $($x),*
            );
        }
    )
}

macro_rules! decl_op_s {
    ($cap_op: ident, $op: ident, $s: ident, $t: ident, $($x: ident),*) => (
        impl std::ops::$cap_op<$s> for $t {
            decl_op_impl_internal_binary_s!(
                $t, $op, $s, $($x),*
            );
        }

        impl<'a> std::ops::$cap_op<$s> for &'a $t {
            decl_op_impl_internal_binary_s!(
                $t, $op, $s, $($x),*
            );
        }

        impl<'b> std::ops::$cap_op<&'b $s> for $t {
            decl_op_impl_internal_binary_s!(
                $t, $op, &'b $s, $($x),*
            );
        }

        impl<'a, 'b> std::ops::$cap_op<&'b $s> for &'a $t {
            decl_op_impl_internal_binary_s!(
                $t, $op, &'b $s, $($x),*
            );
        }
    )
}
