
pub mod color {

    use std::str;
    use data::color::ColorRGB;

    #[derive(Debug)]
    pub enum Error {
        UnrecognizedFormat,
        InvalidHexDigit,
    }

    pub fn read_color(color_str: &str) -> Result< ColorRGB, Error > {
        fn read_short_rgb(color_str: &str) -> Result< ColorRGB, Error > {
            if &color_str[0..1] != "#" {
                Err(Error::UnrecognizedFormat)
            } else {
                match u32::from_str_radix(&color_str[1..], 16) {
                    Err(_) => Err(Error::InvalidHexDigit),
                    Ok(c) => {
                        // set b: 0xrgb -> 0x00000b
                        let mut expended = c & 0x00f;
                        // set g: 0xrgb -> 0x000g0b
                        expended |= (c & 0x0f0) << 4;
                        // set r: 0xrgb -> 0x0r0rgb
                        expended |= (c & 0xf00) << 8;
                        // dup bytes: 0x0r0g0b -> 0xrrggbb
                        expended |= expended << 4;
                        Ok(ColorRGB::from_u32_rgb(&expended))
                    }
                }
            }
        }
        fn read_long_rgb( color_str: &str ) -> Result< ColorRGB, Error > {
            if &color_str[0..1] != "#" {
                Err( Error::UnrecognizedFormat )
            } else {
                match u32::from_str_radix(&color_str[1..], 16) {
                    Err(_) => Err(Error::InvalidHexDigit),
                    Ok(c) => Ok(ColorRGB::from_u32_rgb(&c))
                }
            }
        }
        match color_str.len() {
            4 => read_short_rgb( color_str ),
            7 => read_long_rgb( color_str ),
            _ => Err( Error::UnrecognizedFormat )
        }
    }
}

pub mod material {
    extern crate serde_json;

    use std::fmt;
    use std::str;
    use data::material::Material;
    use data::material::MaterialList;
    use data::color::ColorRGB;

    pub struct KeyTypeError {
        key: String,
        expected_type: String,
    }

    impl fmt::Debug for KeyTypeError {
        fn fmt( &self, f: &mut fmt::Formatter ) -> fmt::Result {
            write!( f, "Key {} as invalid type. Expected type {}.", self.key, self.expected_type )
        }
    }

    pub enum Error {
        ColorError(super::color::Error),
        MissingKey(String),
        InvalidKeyType(KeyTypeError),
        ExpectedMaterial,
    }

    impl Error {
        pub fn fmt( &self, f: &mut fmt::Formatter ) -> fmt::Result {
            write!( f, "Error reading material: " )?;
            match self {
                Error::ColorError( color_error ) => write!( f, "Error reading color: {:?}", color_error ),
                Error::MissingKey( key ) => write!( f, "Missing key \"{}\"", key ),
                Error::InvalidKeyType( key_type_error ) => write!( f, "{:?}", key_type_error ),
                Error::ExpectedMaterial => write!( f, "Expected an object describing the material" ),
            }
        }
    }

    impl fmt::Debug for Error {
        fn fmt( &self, f: &mut fmt::Formatter ) -> fmt::Result {
            self.fmt( f )
        }
    }

    impl fmt::Display for Error {
        fn fmt( &self, f: &mut fmt::Formatter ) -> fmt::Result {
            self.fmt( f )
        }
    }

    pub fn read_material( serialized_material : &serde_json::Map<String, serde_json::Value> )
            -> Result< Material, Error > {
        fn read_color( dict: &serde_json::Map<String, serde_json::Value>, key: &str ) -> Result< ColorRGB, Error > {
            match dict.get( key ) {
                None => Err( Error::MissingKey( key.to_string() ) ),
                Some( value ) => {
                    match value {
                        serde_json::Value::String( value_string ) => {
                            match super::color::read_color( value_string ) {
                                Err( error ) => Err( Error::ColorError( error ) ),
                                Ok( color ) => Ok( color )
                            }
                        }
                        _ => Err( Error::InvalidKeyType( KeyTypeError {
                            key: key.to_string(),
                            expected_type: "String".to_string()
                        } ) ),
                    }
                }
            }
        }
        fn read_float( dict: &serde_json::Map<String, serde_json::Value>, key: &str ) -> Result< f32, Error > {
            match dict.get( key ) {
                None => Err( Error::MissingKey( key.to_string() ) ),
                Some( value ) => {
                    match value {
                        serde_json::Value::Number( num ) => {
                            match num.as_f64() {
                                // TODO: might need a more accurate error
                                None => Err( Error::InvalidKeyType( KeyTypeError {
                                    key: key.to_string(),
                                    expected_type: "Number".to_string()
                                } ) ),
                                Some( float ) => Ok( float as f32 )
                            }
                        }
                        _ => Err( Error::InvalidKeyType( KeyTypeError {
                            key: key.to_string(),
                            expected_type: "Number".to_string()
                        } ) ),
                    }
                }
            }
        }
        let specular_color = read_color( serialized_material, "specular_color" )?;
        let diffuse_color = read_color( serialized_material, "diffuse_color" )?;
        let ambiant_color = read_color( serialized_material, "ambiant_color" )?;
        let reflective_factor = read_float( serialized_material, "reflective_factor" )?;
        let transparent_factor = read_float( serialized_material, "transparent_factor" )?;
        Ok( Material {
            specular_color: specular_color,
            diffuse_color: diffuse_color,
            ambiant_color: ambiant_color,
            reflective_factor: reflective_factor,
            transparent_factor: transparent_factor
        } )
    }

    pub fn read_list( json_value: &serde_json::Value ) -> Result< MaterialList, Error > {
        if let serde_json::Value::Object( json_obj ) = json_value {
            let mut mat_list = MaterialList::new();
            for (key, value) in json_obj {
                if let serde_json::Value::Object( obj ) = value {
                    let mat = read_material( &obj )?;
                    mat_list.store( key.as_ref(), mat );
                } else {
                    return Err( Error::ExpectedMaterial );
                }
            }
            Ok( mat_list )
        } else {
            Err( Error::ExpectedMaterial )
        }
    }
}

pub mod entity {

    extern crate serde_json;

    use std::fmt;
    use data::entities::Sphere;
    use data::material::Material;
    use data::material::MaterialList;
    use data::vector::Vector3;
    use entities::entity_list::EntityList;
    use entities::entity::Entity;

    pub enum EntityType {
        Sphere,
    }

    pub enum Error {
        InvalidMaterialReference,
        UnknownMaterialReference( String ),
        MissingField( String, String ),
        InvalidArrayLength( String ),
        InvalidType( Option< String >, String )
    }

    impl Error
    {
        fn fmt( &self, f: &mut fmt::Formatter ) -> fmt::Result {
            match self {
                Error::InvalidMaterialReference => write!( f, "Invalid material reference: expected a string" ),
                Error::UnknownMaterialReference( r ) => write!( f, "Unknown material '{}'", r ),
                Error::MissingField( obj, field ) => write!( f, "Missing field '{}' for object '{}'", field, obj ),
                Error::InvalidArrayLength( r ) => write!( f, "Invalid array length: {}", r ),
                Error::InvalidType( obj, r ) => {
                    match obj {
                        Some( name ) => write!( f, "Invalid type for field '{}': {}", name, r ),
                        None => write!( f, "Invalid type: {}", r )
                    }
                }
            }
        }
    }

    impl fmt::Debug for Error {
        fn fmt( &self, f: &mut fmt::Formatter ) -> fmt::Result {
            self.fmt( f )
        }
    }

    impl fmt::Display for Error {
        fn fmt( &self, f: &mut fmt::Formatter ) -> fmt::Result {
            self.fmt( f )
        }
    }

    fn read_f32( json_value: &serde_json::Value ) -> Result< f32, Error > {
        if let serde_json::Value::Number( num ) = json_value {
            if let Some( float ) = num.as_f64() {
                Ok( float as f32 )
            } else {
                Err( Error::InvalidType( None, "Number cannot be converted to 64bits float".to_string() ) )
            }
        } else {
            Err( Error::InvalidType( None, "expected number".to_string() ) )
        }
    }

    fn read_vector3( json_value: &serde_json::Value ) -> Result< Vector3, Error > {
        if let serde_json::Value::Array( arr ) = json_value {
            let mut count = 0;
            let mut components = [ 0_f32, 0_f32, 0_f32 ];
            for val in arr {
                if count > components.len() {
                    return Err( Error::InvalidArrayLength( "vector expected 3 elements, got more".to_string() ) );
                }
                components[ count ] = read_f32( val )?;
                count += 1;
            }
            if count < components.len() {
                return Err( Error::InvalidArrayLength( "vector expected 3 elements, got less".to_string() ) );
            }
            Ok( Vector3::new( components[0], components[1], components[2] ) )
        } else {
            Err( Error::InvalidType( Some( "vector".to_string() ), "Expected array".to_string() ) )
        }
    }

    pub fn read_sphere(
            json_value: &serde_json::Value
            ) -> Result< Sphere, Error > {
        if let serde_json::Value::Object( obj ) = json_value {
            let radius = obj.get( "radius" );
            let center = obj.get( "center" );
            if radius.is_none() {
                return Err( Error::MissingField( "sphere".to_string(), "radius".to_string() ) );
            }
            if center.is_none() {
                return Err( Error::MissingField( "sphere".to_string(), "center".to_string() ) );
            }
            let radius = read_f32( radius.unwrap() )?;
            let center = read_vector3( center.unwrap() )?;
            Ok( Sphere { radius: radius, center: center } )
        } else {
            Err( Error::InvalidType( Some( "sphere".to_string() ), "expected object describing sphere".to_string() ) )
        }
    }

    pub fn read_mat_ref(
            json_value: &serde_json::Value,
            material_list: &MaterialList
            ) -> Result< Material, Error > {
        if let serde_json::Value::String( name ) = json_value {
            if let Some( mat_ref ) = material_list.get_material( name ) {
                Ok( mat_ref.clone() )
            } else {
                Err( Error::UnknownMaterialReference( name.to_string() ) )
            }
        } else {
            Err( Error::InvalidMaterialReference )
        }
    }

    pub fn read_entity(
            json_obj: &serde_json::Map< String, serde_json::Value >,
            material_list: &MaterialList,
            entity_list: &mut EntityList
            ) -> Result< (), Error > {
        let mut mat: Option< &serde_json::Value > = None;
        let mut obj: Option< ( EntityType, &serde_json::Value ) > = None;
        for (key, value) in json_obj {
            match key.as_ref() {
                "material" => mat = Some( value ),
                "sphere" => obj = Some( ( EntityType::Sphere, value ) ),
                _ => ()
            }
        }
        if mat.is_none() {
            Err( Error::MissingField( "entity".to_string(), "material".to_string() ) )
        } else if obj.is_none() {
            Err( Error::MissingField( "entity".to_string(), "object describing field (e.g. 'sphere')".to_string() ) )
        } else {
            let mat = mat.unwrap();
            let obj = obj.unwrap();
            let mat = read_mat_ref( mat, material_list )?;
            match obj.0 {
                EntityType::Sphere => {
                    let sphere = read_sphere( obj.1 )?;
                    entity_list.store_sphere( "", Entity::new( sphere, mat ) );
                    Ok(())
                }
            }
        }
    }

    pub fn read_list(
            json_value: &serde_json::Value,
            material_list: &MaterialList
            ) -> Result< EntityList, Error > {
        if let serde_json::Value::Object( json_obj ) = json_value {
            let mut ent_list = EntityList::new();
            for ( key, value ) in json_obj {
                if let serde_json::Value::Object( obj ) = value {
                    read_entity( obj, material_list, &mut ent_list )?;
                } else {
                    return Err( Error::InvalidType( Some( key.to_string() ), "expected an object describing an entity".to_string() ) );
                }
            }
            Ok( ent_list )
        } else {
            Err( Error::InvalidType( None, "expected an object containing the entities".to_string() ) )
        }
    }
}

pub mod scene {
    extern crate serde_json;

    use super::*;
    use std::fmt;
    use std::fs::File;
    use data::material::MaterialList;
    use entities::entity_list::EntityList;

    pub enum Error {
        FileOpeningError( String, String ),
        JsonParsingError( String ),
        ObjectParsingError( String ),
        UnexpectedKey( String ),
        MissingKey( String ),
    }

    impl Error {
        fn fmt( &self, f: &mut fmt::Formatter ) -> fmt::Result {
            match self {
                Error::FileOpeningError( fname, err ) => write!( f, "Error opening {}: {}", fname, err ),
                Error::JsonParsingError( err ) => write!( f, "Error parsing JSON: {}", err ),
                Error::ObjectParsingError( err ) => write!( f, "Error interpreting JSON object: {}", err ),
                Error::UnexpectedKey( key ) => write!( f, "Unexpected key in root object: {}", key ),
                Error::MissingKey( key ) => write!( f, "Missing key {} in root object", key ),
            }
        }
    }

    impl fmt::Debug for Error {
        fn fmt( &self, f: &mut fmt::Formatter ) -> fmt::Result {
            self.fmt( f )
        }
    }

    pub fn read_scene_from_file( filename: &str ) -> Result< ( MaterialList, EntityList ), Error > {
        let fd = File::open( filename );
        if let Err( err ) = fd {
            return Err( Error::FileOpeningError( filename.to_string(), err.to_string() ) );
        }

        let deserialized = serde_json::from_reader( fd.unwrap() );
        if let Err( err ) = deserialized {
            return Err( Error::JsonParsingError( err.to_string() ) );
        }

        if let serde_json::Value::Object( ref json_obj ) = deserialized.unwrap() {
            let mut mat_json_value: Option< &serde_json::Value > = None;
            let mut ent_json_value: Option< &serde_json::Value > = None;
            for ( key, value ) in json_obj {
                match key.as_ref() {
                    "materials" => mat_json_value = Some( value ),
                    "entities" => ent_json_value = Some( value ),
                    _ => return Err( Error::UnexpectedKey( key.to_string() ) )
                }
            }

            if mat_json_value.is_none() {
                return Err( Error::MissingKey( "materials".to_string() ) );
            }
            if ent_json_value.is_none() {
                return Err( Error::MissingKey( "entities".to_string() ) );
            }

            let material_list = material::read_list( mat_json_value.unwrap() );
            if let Err( err ) = material_list {
                return Err( Error::ObjectParsingError( err.to_string() ) );
            }

            let material_list = material_list.unwrap();
            let entity_list = entity::read_list( ent_json_value.unwrap(), &material_list );
            if let Err( err ) = entity_list {
                return Err( Error::ObjectParsingError( err.to_string() ) );
            }
            let entity_list = entity_list.unwrap();

            Ok( ( material_list, entity_list ) )
        } else {
            Err( Error::ObjectParsingError( "root element must be an object".to_string() ) )
        }
    }
}

#[cfg(test)]
mod tests {
    extern crate serde_json;

    use super::*;
    use data::color::ColorRGB;
    use data::entities::Sphere;
    use data::vector::Vector3;

    #[test]
    fn test_read_color_short() {
        let c = color::read_color( "#abc" ).unwrap();
        let t = ColorRGB::from_u32_rgb( &0xaabbcc );

        assert_eq!( c, t );
    }

    #[test]
    fn test_read_color_long() {
        let c = color::read_color( "#A0B1C2" ).unwrap();
        let t = ColorRGB::from_u32_rgb( &0xa0b1c2 );

        assert_eq!( c, t);
    }

    #[test]
    fn test_read_material() {
        let serialized = r###"{
                "specular_color": "#fff",
                "diffuse_color": "#f0e1d2",
                "ambiant_color": "#fff",
                "reflective_factor": 1,
                "transparent_factor": 0.2
            }"###;
        let deserialized = serde_json::from_str( &serialized );
        match deserialized {
            Ok( json_obj ) => match json_obj {
                serde_json::Value::Object( object ) => {
                    let mat = material::read_material( &object ).unwrap();
                    let spec = ColorRGB::from_u32_rgb( &0xffffff );
                    let diff = ColorRGB::from_u32_rgb( &0xf0e1d2 );
                    let ambi = ColorRGB::from_u32_rgb( &0xffffff );
                    assert_eq!( spec, mat.specular_color );
                    assert_eq!( diff, mat.diffuse_color );
                    assert_eq!( ambi, mat.ambiant_color );
                    assert_eq!( 1.0_f32, mat.reflective_factor );
                    assert_eq!( 0.2_f32, mat.transparent_factor );
                },
                _ => panic!( "Error in test: could not deserialize JSON payload properly" )
            }
            Err( err ) => panic!( "Error in test: could not deserialize JSON payload:\n{}", err )
        }
    }

    #[test]
    fn test_read_material_list() {
        let serialized = r###"{
            "mat0": {
                "specular_color": "#fff",
                "diffuse_color": "#f0e1d2",
                "ambiant_color": "#fff",
                "reflective_factor": 0,
                "transparent_factor": 0.2
            },
            "mat1": {
                "specular_color": "#fff",
                "diffuse_color": "#f0e1d2",
                "ambiant_color": "#fff",
                "reflective_factor": 1,
                "transparent_factor": 0.2
            },
            "mat2": {
                "specular_color": "#fff",
                "diffuse_color": "#f0e1d2",
                "ambiant_color": "#fff",
                "reflective_factor": 2,
                "transparent_factor": 0.2
            }
        }"###;
        match serde_json::from_str( &serialized ) {
            Ok( json_value ) => {
                let mat_list = material::read_list( &json_value ).unwrap();
                assert_eq!( mat_list.get_material( "mat0" ).unwrap().reflective_factor, 0_f32 );
                assert_eq!( mat_list.get_material( "mat1" ).unwrap().reflective_factor, 1_f32 );
                assert_eq!( mat_list.get_material( "mat2" ).unwrap().reflective_factor, 2_f32 );
            },
            Err( err ) => panic!( "Error in test: failed to deserialize JSON payload\n{}", err )
        }
    }

    #[test]
    fn test_read_sphere() {
        let expected = Sphere {
            radius: 1.0_f32,
            center: Vector3 { x: 10_f32, y: 0.5_f32, z: -0.33_f32 }
        };
        let serialized = r###"{
            "radius": 1.0,
            "center": [ 10, 0.5, -0.33 ]
        }"###;
        let deserialized = serde_json::from_str( &serialized );
        match deserialized {
            Ok( json_value ) => {
                let sphere = entity::read_sphere( &json_value );
                assert!( sphere.is_ok() );
                assert_eq!( sphere.unwrap(), expected );
            }
            Err( err ) => panic!( "Error in test: failed to deserialize JSON payload\n{}", err )
        }
    }
}
