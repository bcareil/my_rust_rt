use std::io;
use std::mem;

pub struct Buffer {
    w: usize,
    h: usize,
    data: Vec<u32>,
}

impl Buffer {
    pub fn new(w: usize, h: usize) -> Buffer {
        Buffer { w: w, h: h, data: vec![0; w * h] }
    }

    /// Write itself to a stream in the binary PPM format
    ///
    /// The format expected in the data member is RGB as in 0xXXRRGGBB
    /// where XX is discarded.
    pub fn to_ppm6<T: io::Write>(&self, out: &mut T) -> io::Result<()> {
        write!(out, "P6\n{} {}\n255\n", self.w, self.h)?;
        self.to_rgb_stream(out)?;
        Ok(())
    }

    /// Write the data member in a stream in the RGB format.
    ///
    /// The format expected in the data member is RGB as in 0xXXRRGGBB
    /// where XX is discarded.
    pub fn to_rgb_stream<T: io::Write>(&self, out: &mut T) -> io::Result<()> {
        for pixel in &self.data {
            let pixel: &[u8; 4] = &unsafe { mem::transmute::<u32, [u8; 4]>(pixel.to_be()) };
            let pixel: &[u8] = &pixel[1..4];
            match out.write_all(pixel) {
                Err(x) => { return Err(x) },
                _ => {},
            }
        }
        Ok(())
    }

    /// Return buffer width
    pub fn get_width(&self) -> usize {
        self.w
    }

    /// Return buffer height
    pub fn get_height(&self) -> usize {
        self.h
    }

    /// Set pixel colors. Color should be 0xXXRRGGBB.
    pub fn set_pixel(&mut self, x: usize, y: usize, color: u32) {
        self.data[y * self.w + x] = color;
    }
}

