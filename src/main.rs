
extern crate serde_json;

mod data;
mod entities;
mod utils;
mod lightning;

use std::fs::File;
use data::buffer::Buffer;
use data::material::Material;
use data::vector::Vector3;
use data::serialization::scene;

fn main() {
    let mut b = Buffer::new(64*8, 64*8);
    let (mut materials, entities ) = scene::read_scene_from_file( "scenes/scene-00.json" ).unwrap();

    // apply global factors
    materials.apply(| mat: &mut Material | {
        mat.specular_color = &mat.specular_color * 0.75;
        mat.diffuse_color = &mat.diffuse_color * 0.9;
        mat.ambiant_color = &mat.ambiant_color * 0.0635;
        mat.reflective_factor *= 0.0;
        mat.transparent_factor *= 0.0;
    } );

    let bw = b.get_width();
    let bh = b.get_height();
    let dim = &Vector3::new(bw as f32, bh as f32, 1.0);
    let middle = &Vector3::new(dim.x / 2.0, dim.y / 2.0, 1.0);
    let org = &Vector3::new(0.0, 0.0, -5.0);
    let light_source = &Vector3::new(-1.0, 5.0, -2.0);
    for y in 0..bh {
        for x in 0..bw {
            let dir = {
                let current = &Vector3::new((bw - x) as f32, y as f32, 0.0);
                let relative = (middle - current) / dim;
                relative.normalize()
            };

            let mut closest = std::f32::MAX;
            for object in entities.get_spheres() {
                match object.compute_distance(org, &dir) {
                    Some(dist) => {
                        if dist < closest {
                            closest = dist;
                            let intersection = org + &dir * dist;
                            let light_vector = (light_source - &intersection).normalize();
                            let normal = object.compute_normal(&intersection);
                            let color = object.compute_lightning(&light_vector, &dir, &normal);
                            b.set_pixel(x, y, color.bleed().to_u32_rgb())
                        }
                    },
                    None => { /* NOP */ }
                }
            }
        }
    }

    let mut f = File::create("foo.ppm").unwrap();
    b.to_ppm6(&mut f).unwrap();
}
