
use std::cmp::Ordering;
use data::vector::Vector3;
use utils::float_cmp_ignore_spec;

pub fn phong_factor(
    light_vector: &Vector3,
    ray_dir: &Vector3,
    normal: &Vector3
    ) -> f32 {
    // k = light . (ray_dir - 2normal(normal . ray_dir))
    // k = k^n
    let normal_dot_ray = normal.dot(ray_dir);
    let normal_2 = normal * 2.0;
    let k = light_vector.dot(&(ray_dir - normal_2 * normal_dot_ray));
    match float_cmp_ignore_spec(k, 0.0, 0.001) {
        Ordering::Greater => {
            let k_2 = k * k;
            let k_4 = k_2 * k_2;
            let k_8 = k_4 * k_4;
            k_8 * k_4
        },
        _ => 0.0
    }
}

pub fn diffuse_factor(
    light_vector: &Vector3,
    normal: &Vector3
    ) -> f32 {
    let normal_dot_light = normal.dot(light_vector);
    match float_cmp_ignore_spec(normal_dot_light, 0.0, 0.001) {
        Ordering::Greater => normal_dot_light,
        _ => 0.0,
    }
}

