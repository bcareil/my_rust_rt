
use std::cmp::Ordering;

#[allow(dead_code)]
#[derive(Debug,PartialEq)]
pub enum FloatOrderingUnordered {
    EitherOrBothNan,
    BothSameInfinit,
    Less,
    Equal,
    Greater,
}

#[allow(dead_code)]
pub fn float_cmp(a: f32, b: f32, e: f32) -> FloatOrderingUnordered {
    if a.is_nan() || b.is_nan() {
        FloatOrderingUnordered::EitherOrBothNan
    } else if a.is_infinite() && b.is_infinite() && a.signum() == b.signum() {
        FloatOrderingUnordered::BothSameInfinit
    } else if (a - b).abs() < e {
        FloatOrderingUnordered::Equal
    } else if a < b {
        FloatOrderingUnordered::Less
    } else {
        FloatOrderingUnordered::Greater
    }
}

pub fn float_cmp_ignore_spec(a: f32, b: f32, e: f32) -> Ordering {
    let tmp = (a - b).abs();
    if tmp < e {
        Ordering::Equal
    } else if a < b {
        Ordering::Less
    } else {
        Ordering::Greater
    }
}

