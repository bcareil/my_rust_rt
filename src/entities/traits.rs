
use data::vector::Vector3;

pub trait Intersect {
    fn compute_distance(
        &self,
        ray_org: &Vector3,
        ray_dir: &Vector3
        ) -> Option< f32 >;

    fn compute_normal(
        &self,
        intersection: &Vector3
        ) -> Vector3;
}
