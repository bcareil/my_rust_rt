
use data::vector::Vector3;
use data::color::ColorRGB;
use data::material::Material;
use entities::traits::Intersect;
use lightning;

pub struct Entity< T: Intersect > {
    material: Material,
    entity: T,
}

impl<T: Intersect> Entity<T> {

    pub fn new( entity: T, mat: Material ) -> Entity< T > {
        Entity {
            material: mat,
            entity: entity,
        }
    }

    pub fn compute_lightning(
        &self,
        light_vector: &Vector3,
        ray_dir: &Vector3,
        normal: &Vector3
    ) -> ColorRGB {
        let phong_factor = lightning::phong_factor(light_vector, ray_dir, normal);
        let diffuse_factor = lightning::diffuse_factor(light_vector, normal);

        (&self.material.specular_color * phong_factor) +
            (&self.material.diffuse_color * diffuse_factor) +
            (&self.material.ambiant_color)
    }

    pub fn compute_distance(
        &self,
        ray_org: &Vector3,
        ray_dir: &Vector3
    ) -> Option<f32> {
        self.entity.compute_distance( ray_org, ray_dir )
    }

    pub fn compute_normal(
        &self,
        intersection: &Vector3
    ) -> Vector3 {
        self.entity.compute_normal( intersection )
    }

}

impl< T: Intersect > Intersect for Entity< T > {
    fn compute_distance(
        &self,
        ray_org: &Vector3,
        ray_dir: &Vector3
    ) -> Option<f32> {
        self.compute_distance( ray_org, ray_dir )
    }

    fn compute_normal(
        &self,
        intersection: &Vector3
    ) -> Vector3 {
        self.compute_normal( intersection )
    }
}
