
use data::entities::Sphere;
use data::vector::Vector3;
use entities::traits::Intersect;
use utils::float_cmp_ignore_spec;
use std::cmp::Ordering;

impl Intersect for Sphere {
    fn compute_distance(
            &self,
            ray_org: &Vector3,
            ray_dir: &Vector3
            ) -> Option< f32 > {
        let center_to_org = ray_org - &self.center;
        let center_to_org_dist_sq = center_to_org.length_sq();
        // solve quadratic formula: ax^2 + bx + c = 0 where
        // a = ||dir||^2 = 1.0 // (dir is a unit vector)
        // b = 2 (dir . (org - center))
        // c = ||org - center||^2 - radius^2
        // delta = b^2 - 4ac
        let radius_sq = self.radius * self.radius;
        let b = 2.0 * ray_dir.dot(&center_to_org);
        let c = center_to_org_dist_sq - radius_sq;
        let delta = (b * b) - (4.0 * c);

        match float_cmp_ignore_spec(delta, 0.0, 0.001) {
            // delta < 0 => no solution
            Ordering::Less    => None,
            // delta = 0 => solution x = -b / 2a
            Ordering::Equal   => if b < 0.0 { Some(-b / 2.0) } else { None },
            // delta > 0 => two solutions
            Ordering::Greater =>  {
                let delta_sqrt = delta.sqrt();
                let k1 = (-delta_sqrt - b) / 2.0;
                let k2 = (delta_sqrt - b) / 2.0;
                if k1 < k2 && k1 > 1.0 {
                    Some(k1)
                } else {
                    Some(k2)
                }
            }
        }
    }

    fn compute_normal(
            &self,
            intersection: &Vector3
            ) -> Vector3
    {
        ( intersection - &self.center ).normalize()
    }
}

#[cfg(test)]
mod tests {

    use super::*;
    use utils::float_cmp;
    use utils::FloatOrderingUnordered;

    #[test]
    fn test_sphere_dist() {
        let sphere = Sphere {
            radius: 1.0_f32,
            center: Vector3 { x: 5.0_f32, y: 0.0_f32, z: 0.0_f32 }
        };
        let ray_org = Vector3 {
            x: 0.0_f32, y: 0.0_f32, z: 0.0_f32
        };
        let ray_dir_sphere = Vector3 {
            x: 1.0_f32, y: 0.0_f32, z: 0.0_f32
        };

        if let Some( dist ) = sphere.compute_distance( &ray_org, &ray_dir_sphere ) {
            assert_eq!( float_cmp( dist, 4.0, 0.001 ), FloatOrderingUnordered::Equal );
        } else {
            panic!( "Failed to intersect sphere" );
        }
    }

    #[test]
    fn test_sphere_dist_none() {
        let sphere = Sphere {
            radius: 1.0_f32,
            center: Vector3 { x: 5.0_f32, y: 0.0_f32, z: 0.0_f32 }
        };
        let ray_org = Vector3 {
            x: 0.0_f32, y: 0.0_f32, z: 0.0_f32
        };
        let ray_dir_void = Vector3 {
            x: 0.0_f32, y: 1.0_f32, z: 0.0_f32
        };

        assert!( sphere.compute_distance( &ray_org, &ray_dir_void ).is_none() );
    }
}
