
use data::entities::Sphere;
use entities::entity::Entity;

pub struct EntityList {
    spheres: Vec< Entity< Sphere > >,
    sphere_names: Vec< String >,
}

impl EntityList {
    pub fn new() -> EntityList {
        EntityList { spheres: Vec::new(), sphere_names: Vec::new() }
    }

    pub fn store_sphere( &mut self, name: &str, sphere: Entity< Sphere > ) {
        self.spheres.push( sphere );
        self.sphere_names.push( name.to_string() );
    }

    pub fn get_spheres( &self ) -> &Vec< Entity< Sphere > > {
        &self.spheres
    }
}
